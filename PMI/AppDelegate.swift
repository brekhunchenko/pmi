//
//  AppDelegate.swift
//  PMI
//
//  Created by Yaroslav Brekhunchenko on 11/16/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        #if LAMP
            print("LAMP APP STARTED.")
        #else
            print("POS APP STARTED.")
        #endif
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        let shouldLogin : Bool = PMISessionManager.defaultManager.hostessId == nil
        if (shouldLogin) {
            self.window!.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController")
        } else {
            self.window!.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController")
        }
        
        self.window?.makeKeyAndVisible()

        return true
    }

}

