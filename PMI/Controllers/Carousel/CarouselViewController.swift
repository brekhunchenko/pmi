//
//  CarouselViewController.swift
//  PMI
//
//  Created by Yaroslav Brekhunchenko on 11/16/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit
import iCarousel
import PINCache

class CarouselViewController: BaseViewController {
    
    @IBOutlet weak var carouselView: iCarousel!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var winTextImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var redScquareView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var userSwipped : Bool = false
    
    //MARK: UIViewController Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCarousel()
        self.setupAppearance()
//        self.setupGestureRecognizers()
    }
    
    //MARK: Setup
    
    func setupGestureRecognizers() {
        let swipeLeft : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGesureCatched))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        
        let swipeRight : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeGesureCatched))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func setupCarousel() {
        self.carouselView.type = iCarouselType.rotary
        self.carouselView.stopAtItemBoundary = true
        
        self.redScquareView.backgroundColor = UIColor.clear
        if let color: UIColor = PMIDataSource.defaultDataSource.activeCampaign()?.theme?.carouselBorderColor {
            self.redScquareView.layer.borderColor = color.cgColor
        }
        self.redScquareView.layer.cornerRadius = 8.0
        self.redScquareView.layer.borderWidth = 7.0
        self.redScquareView.layer.masksToBounds = true
    }
    
    func setupAppearance() {
        if let theme = PMIDataSource.defaultDataSource.activeCampaign()?.theme {
            let startImage = PINCache.shared.diskCache.object(forKey: theme.startButtonImageURL!.pinCacheStringKey()) as? UIImage
            self.playButton.setImage(startImage, for: .normal)
            
            let endImage = PINCache.shared.diskCache.object(forKey: theme.endButtonImageURL!.pinCacheStringKey()) as? UIImage
            self.confirmButton.setImage(endImage, for: .normal)
            
            let backgroundImage = PINCache.shared.diskCache.object(forKey: theme.backgroundImageURL!.pinCacheStringKey()) as? UIImage
            self.backgroundImageView.image = backgroundImage
        }
    }
    
    //MARK: Actions
    
    @IBAction func confirmButtonAction(_ sender: Any) {
        let giftScenarion : GiftScenario! = PMIDataSource.defaultDataSource.activeCampaign()?.activeScenario!.getNextGiftScenario()!
        giftScenarion.distributed = true
        giftScenarion.distributionTime = NSDate()
        do {
            try PMIDataSource.defaultDataSource.managedObjectContext.save()
            self.navigationController?.popViewController(animated: true)
        } catch {
            let alertController = UIAlertController(title: "Error.", message: "Something went wrong, please try again.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func playButtonAction(_ sender: Any) {
        if (self.userSwipped == false) {
            self.userSwipped = true
            UIView.animate(withDuration: 0.5, animations: {
                self.playButton.alpha = 0.0
            })
            
            let currIndex = self.carouselView.currentItemIndex
            let numItems = self.carouselView.numberOfItems
            let timesAround = 2
            let currentGiftScenarioCarouselIndex : Int = self.currentGiftScenarioCarouselIndex()
            let numToScroll = timesAround*numItems + (currentGiftScenarioCarouselIndex - currIndex)
            
            self.carouselView.scroll(byNumberOfItems: numToScroll, duration: 5.0)
        }
    }
    
    @objc func swipeGesureCatched(gestureRecognizer: UISwipeGestureRecognizer) {
        if ((gestureRecognizer.state == .ended || gestureRecognizer.state == .cancelled) && self.userSwipped == false) {
            let currentGiftScenarioCarouselIndex : Int = self.currentGiftScenarioCarouselIndex()
            if (currentGiftScenarioCarouselIndex != NSNotFound) {
                self.userSwipped = true

                let currIndex = self.carouselView.currentItemIndex
                let numItems = self.carouselView.numberOfItems
                let timesAround = 1
                var numToScroll = 0
                
                switch gestureRecognizer.direction {
                    case UISwipeGestureRecognizerDirection.right:
                        numToScroll = -(2*numItems + (currIndex - currentGiftScenarioCarouselIndex))
                    case UISwipeGestureRecognizerDirection.left:
                        numToScroll = timesAround*numItems + (currentGiftScenarioCarouselIndex - currIndex)
                    default:
                        break
                }

                self.carouselView.scroll(byNumberOfItems: numToScroll, duration: 1.7)
            }
        }
    }
    
}

extension CarouselViewController : iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        let listOfGifts : [Gift] = PMIDataSource.defaultDataSource.activeCampaign()!.activeScenario!.listOfAllGifts()
        return listOfGifts.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var carouselView : GiftView? = view as! GiftView?
        if (carouselView == nil) {
            carouselView = GiftView.instanceFromNib()
        }
        let gift : Gift = PMIDataSource.defaultDataSource.activeCampaign()!.activeScenario!.listOfAllGifts()[index]
        carouselView?.gift = gift
        return carouselView!
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch option {
            case iCarouselOption.spacing:
                return value
        case iCarouselOption.arc:
                return value*0.5
            case iCarouselOption.visibleItems:
                return 5
            
            default: break
        }
        
        return value
    }
    
    func carouselDidEndDragging(_ carousel: iCarousel, willDecelerate decelerate: Bool) {
        if (decelerate) {
            self.userSwipped = true
        }
    }
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        if (userSwipped) {
            self.showWonGiftAnimation()
        }
    }
    
    func showWonGiftAnimation() {
        let giftView = GiftView.instanceFromNib()
        let giftScenarion : GiftScenario! = PMIDataSource.defaultDataSource.activeCampaign()!.activeScenario!.getNextGiftScenario()
        giftView.gift = giftScenarion.gift!
        giftView.center = CGPoint(x: self.carouselView.bounds.size.width/2.0, y: self.carouselView.bounds.size.height/2.0)
//        giftView.dropShadow()
        giftView.alpha = 0.0
        self.view.addSubview(giftView)
    
        UIView.animate(withDuration: 0.5, animations: {
            giftView.alpha = 1.0
        })
        
        UIView.animate(withDuration: 1.0, animations: {
            self.carouselView.alpha = 0.0
            self.playButton.alpha = 0.0
            self.redScquareView.alpha = 0.0
        }) { (completed) in
            UIView.animate(withDuration: 1.0, animations: {
                giftView.center = CGPoint(x: self.carouselView.bounds.size.width/2.0, y: self.carouselView.bounds.size.height/2.0 - 150.0)
                giftView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                
                self.winTextImageView.alpha = 1.0
                self.confirmButton.alpha = 1.0
            })
        }
    }
    
    func currentGiftScenarioCarouselIndex() -> Int {
        let giftScenarion : GiftScenario! = PMIDataSource.defaultDataSource.activeCampaign()!.activeScenario!.getNextGiftScenario()!
        let currentGift : Gift = giftScenarion.gift!
        for i in 0 ..< PMIDataSource.defaultDataSource.activeCampaign()!.activeScenario!.listOfAllGifts().count {
            let gift : Gift = PMIDataSource.defaultDataSource.activeCampaign()!.activeScenario!.listOfAllGifts()[i]
            if (gift.giftID == currentGift.giftID) {
                return i
            }
        }
        assert(false, "Invalid current scenario. Unable to current gift carousel image.")
        return NSNotFound
    }
    
}
