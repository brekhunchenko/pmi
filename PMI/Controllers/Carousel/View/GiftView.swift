//
//  GiftView.swift
//  PMI
//
//  Created by Yaroslav Brekhunchenko on 11/16/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit
import PINCache

class GiftView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var gift : Gift? {
        didSet {
            let imageName : String = (gift?.image)!
            imageView.image = PINCache.shared.diskCache.object(forKey: imageName.pinCacheStringKey()) as! UIImage!
        }
    }
    
    class func instanceFromNib() -> GiftView {
        let view : GiftView = UINib(nibName: "GiftView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! GiftView
        view.frame = CGRect(x: 0.0, y: 0.0, width: 320.0, height: 320.0)
        view.backgroundColor = UIColor.clear
        view.layer.borderColor = UIColor(red: 108.0/255.0, green: 108.0/255.0, blue: 108.0/255.0, alpha: 1.0).cgColor
        view.layer.cornerRadius = 8.0
        view.layer.borderWidth = 6.0
        view.layer.masksToBounds = true
        return view
    }
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 4
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds.insetBy(dx: -4.0, dy: -4.0)).cgPath
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
}

