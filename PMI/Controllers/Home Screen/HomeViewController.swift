//
//  HomeViewController.swift
//  PMI
//
//  Created by Yaroslav Brekhunchenko on 11/16/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit
import PINCache

class HomeViewController: BaseViewController {

    @IBOutlet weak var startButton: StartButton!
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    var showedNextScreen : Bool = false
    
    //MARK: UIViewController Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupGestures()
        self.setupAppearance()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.showedNextScreen = false
        self.setupAppearance()
    }
    
    //MARK: Setup
    
    func setupAppearance() {
        self.startButton.adjustsImageWhenHighlighted = false
        if let theme = PMIDataSource.defaultDataSource.activeCampaign()?.theme {
            let tuDecidesImage = PINCache.shared.diskCache.object(forKey: theme.homeImageURL!.pinCacheStringKey())
            if (tuDecidesImage != nil) {
                self.startButton.setBackgroundImage(tuDecidesImage as? UIImage, for: .normal)
            }
            let backgroundImage = PINCache.shared.diskCache.object(forKey: theme.backgroundImageURL!.pinCacheStringKey()) as? UIImage
            self.backgroundImageView.image = backgroundImage
        }
    }
    
    func setupGestures() {
        let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureRecognizerCatched))
        tapGesture.numberOfTapsRequired = 3
        self.view.addGestureRecognizer(tapGesture)
    }
    
    //MARK: Gestures
    
    @objc func tapGestureRecognizerCatched(gestureRecognizer: UITapGestureRecognizer) {
        let location : CGPoint = gestureRecognizer.location(in: self.view)
        if (location.x > self.view.bounds.size.width*0.7 && location.y < self.view.bounds.size.height*0.3) {
            let alertController = UIAlertController(title: "Please enter password", message: "", preferredStyle: .alert)

            let saveAction = UIAlertAction(title: "Enter", style: .default, handler: {
                alert -> Void in

                let firstTextField = alertController.textFields![0] as UITextField
                firstTextField.isSecureTextEntry = true
                if (firstTextField.text == "9379992" || firstTextField.text == PMISessionManager.defaultManager.password) {
                    DispatchQueue.main.async {
                        let vc : ConfigurationViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfigurationViewController") as! ConfigurationViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    let alertController = UIAlertController(title: "Error.", message: "Mot de passe incorrect", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            })

            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (action : UIAlertAction!) -> Void in

            })

            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Password"
                textField.keyboardType = UIKeyboardType.numberPad
                textField.isSecureTextEntry = true
            }

            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)

            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //MARK: Actions
    
    @IBAction func startButtonAction(_ sender: Any) {
        if (self.showedNextScreen == false) {
            self.showedNextScreen = true
            
            let activeCampaign: Campaign? = PMIDataSource.defaultDataSource.activeCampaign()
            let activeScenario: Scenario? = activeCampaign?.activeScenario
            if (activeScenario == nil || activeScenario!.notDistributedGiftScenarios().count == 0) {
                let alertController = UIAlertController(title: "Plus de cadeaux à distribuer sur ce channel.", message: "", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                let vc : CarouselViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CarouselViewController") as! CarouselViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
