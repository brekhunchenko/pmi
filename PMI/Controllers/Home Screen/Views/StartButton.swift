//
//  StartButton.swift
//  PMI
//
//  Created by Yaroslav Brekhunchenko on 11/28/17.
//  Copyright © 2017 Yaroslav Brekhunchenko. All rights reserved.
//

import UIKit

class StartButton: UIButton {

    override var isHighlighted: Bool {
        willSet {
            if isHighlighted == false {
                self.alpha = 0.9
            } else {
                self.alpha = 1.0
            }
        }
    }
    
}
